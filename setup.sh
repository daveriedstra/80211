#!/usr/bin/env bash
echo "802.11 rpi setup script initializing. This will install jack, puredata,
python-osc, and scapy\n"

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# need to install:

# jack
jackd_needle=$(dpkg -l | grep -e "jackd\s")
if [[ -z $jackd_needle ]]; then
        echo "could not find jackd, installing..."
        apt-get install jackd
        echo " jackd installed."
else
        echo "jackd already installed..."
fi

# puredata
pd_l2ork_needle=$(dpkg -l | grep "puredata")
if [[ -z $pd_l2ork_needle ]]; then
        echo " could not find puredata, installing..."
        # mkdir pd && cd pd
        # wget http://l2ork.music.vt.edu/data/pd/pd-l2ork-armv6l-20160614.deb
        # dpkg -i pd-l2ork-armv6l-20160614.deb
        # apt-get update && apt-get -f install
        apt-get install -y puredata
        echo " puredata installed."
else
        echo " puredata already installed..."
fi

# python-osc
python_osc_needle=$(pip3 list | grep -e "python-osc\s")
if [[ -z $python_osc_needle ]]; then
        echo " could not find python-osc, installing..."
        pip3 install python-osc
        echo " python-osc installed."
else
        echo " python-osc already installed..."
fi

# scapy
python3 -c "import scapy"
if [[ "$?" == "1" ]]; then
        echo " could not find scapy, installing..."
        # install scapy from git
        mkdir tmp && cd tmp
        git clone https://github.com/secdev/scapy scapy
        cd scapy
        python3 ./setup.py install
        echo " scapy installed."
else
        echo " scapy already installed..."
fi

echo "\nInstallation complete. You may now enjoy 802.11.\n"

