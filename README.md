# 802.11 (2016-2018)
## Sound installation for spatially sonifying environmental wifi
_for more info, visit [daveriedstra.com/802.11](https://www.daveriedstra.com/802.11)_

### Hardware / dependencies:
* Raspberry Pi 3 (tested running Raspbian Stretch, might work on others)
* A WiFi interface which supports monitor mode
* Python 3
**the remaining dependencies are installed during setup:**
* jackd
* puredata
* python-osc
* scapy

### Usage:
* clone this repo and enter directory
* set up by running the setup script `./setup.sh`
    * this will install jackd, puredata, python-osc, scapy
* determine name of wifi card (typically `wlan0` or `wlan1` if predictable NIC names is off)
* run `./start.sh [your-nic-name]`
* stop with `killall python3` or by spamming `SIGINT` in the same session

### What `start.sh` does
* sets the interface to Monitor mode
* starts jackd (logs output in `logs/jackd.log`)
    * _this 1 weird trick was necessary to make jackd play nice with a headless setup (ie, without an X session), take a look in the script as it might be useful for other purposes_
* opens `main.pd` in puredata and starts DSP (logs output in `logs/pd.log`)
* starts `listen.py` (no logs)
* kills puredata and jackd when `listen.py` is done

### What `listen.sh` does
* captures the given number of packets (default infinity), sends control data representing each packet over OSC to the given address (default `localhost:5050`)
* hops to different channels in the 2.4GHz band at random

### What `main.pd` does
* listens on `localhost:5050` for incoming OSC messages
* generates sounds representing these messages
