#!/usr/bin/env python3

from math import log10
import random
import argparse
import logging
import re
import subprocess
from pythonosc import udp_client

# needed for scapy IPv6 weirdness
# https://stackoverflow.com/a/24813180/2053087
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import sniff, struct, Dot11, ls, RadioTap


# used when channel-hopping to determine how much time to spend
# sniffing on each channel
MAX_CHANNEL_TIME = 5
MIN_CHANNEL_TIME = 0.25


def get_args():
    """set script arguments"""
    parser = argparse.ArgumentParser(
            description="Capture packets and send them over OSC")
    parser.add_argument("-c", "--count", action="store", type=int,
                        default=0,
                        help="Set the number of packets to capture")
    parser.add_argument("-i", "--iface", action="store",
                        help="Set the interface on which to listen")
    parser.add_argument("-a", dest="osc_address", action="store",
                        default="localhost", help="Set the OSC target address")
    parser.add_argument("-p", dest="osc_port", action="store", default=5050,
                        type=int, help="Set the OSC target port")
    parser.add_argument("-b", dest="hop", action="store_true",
                        help="Whether to channel hop / bounce. If set, ignores\
                        --count")
    parser.add_argument("-y", dest="verbose", action="store_true",
                        help="log packets as they come in")
    parser.add_argument("-t", dest="tplink", action="store_true",
                        help="Use TP-Link NIC (defaults to Panda)")
    return parser.parse_args()


class PacketDetails:
    signal = 0
    length = 0
    type = 0
    type_str = ""
    subtype = 0
    TA = ""


def sig_to_velocity(sig: int):
    """estimate distance and give a 0-127 velocity

    use FSPL to estimate distance, scale to meaningful distance,
    invert, curve logarithmically, scale to midi (127)
    """
    global freq
    exp = (27.55 - (20 * log10(freq)) + abs(sig)) / 18.0
    dist = 10**exp

    # say our effective range is 95  metres
    # and we want to scale to 127 and invert
    # (min caps to 127)
    e_range = 10
    closeness = 1 - (min(dist, e_range) / e_range)

    return log10((closeness * 9) + 1) * 127


def send_osc_packet(pkt: PacketDetails):
    """formats an osc message representing a packet and sends it

    Sends an OSC message representing one packet to the pd patch.
    format: pitch, velocity, duration, "dirt"
    """
    global chan

    pitch = pkt.subtype
    # 16 sub-types = 16th division of 8ve
    # (sub-types are base-0)
    pitch = pitch / 15 * 12

    # 14 channels in 2.4 ghz band = 14 transpositions
    pitch += ((chan - 1) / 13)

    vel = sig_to_velocity(pkt.signal)
    dur = pkt.length
    dirt = pkt.length / 1500  # effective max frame length

    # manipulate data for each frame type
    # pitch *= changes band; += transposes
    if pkt.type_str == "Data":
        pitch *= 2
        pitch += (12 * 8.3)
    elif pkt.type_str == "Management":
        pitch += (12 * 3.4)
        dirt = 0
    elif pkt.type_str == "Control":
        pitch *= 1.7
        pitch += (12 * 4.4)
        vel = max(vel * 3, 127)

    send_osc_msg("/IEEE80211/%s" % pkt.type_str, [pitch, vel, dur, dirt])


def send_osc_msg(address: str, msg):
    """Sends an osc message"""
    global osc_client
    osc_client.send_message(address, msg)


def type_string(type: int):
    if type == 0:
        return "Management"
    elif type == 1:
        return "Control"
    elif type == 2:
        return "Data"
    else:
        return "Reserved"


def scapy_cb(pkt):
    """production callback for scapy"""
    try:
        if pkt.haslayer(Dot11):
            process_pkt(pkt)
    except Exception as e:
        print(e)


def scapy_cb_tally(pkt):
    """dev callback for scapy; shows statistics"""
    try:
        if pkt.haslayer(Dot11):  # and pkt.addr2 == "84:10:0d:b3:3c:f5":
            tally_packet(process_pkt(pkt))
    except Exception as e:
        print(e)


def process_pkt(pkt):
    global get_dbm_antsignal
    pkt_deets = PacketDetails()
    pkt_deets.signal = get_dbm_antsignal(pkt.notdecoded)
    pkt_deets.length = len(pkt)
    pkt_deets.type = pkt.type
    pkt_deets.type_str = type_string(pkt.type)
    pkt_deets.subtype = pkt.subtype
    pkt_deets.TA = pkt.addr2
    send_osc_packet(pkt_deets)
    return pkt_deets


def init_tally():
    global tally
    tally = [[], [], [], []]
    for type_tally in tally:
        for j in range(16):
            type_tally.append({"count": 0, "sig_sum": 0, "len_sum": 0})


def tally_packet(pkt_deets: PacketDetails):
    """adds each packet to the tally to show when execution is finished."""
    global tally, transmitters, args
    transmitters[pkt_deets.TA] = 1
    tally[pkt_deets.type][pkt_deets.subtype]["count"] += 1
    tally[pkt_deets.type][pkt_deets.subtype]["sig_sum"] += pkt_deets.signal
    tally[pkt_deets.type][pkt_deets.subtype]["len_sum"] += pkt_deets.length
    if args.verbose:
        print("{0}\tfrom {1}\tlen: {2}\t{3}dbm"
              .format(pkt_deets.type_str, pkt_deets.TA, pkt_deets.length,
                      pkt_deets.signal))


def print_tally():
    """prints the tally totals"""
    print("Thanks for playing, here's your score:")
    global tally, transmitters
    print("Found {0} transmitters:".format(len(transmitters.keys())))
    print(transmitters.keys())
    for i, type_tally in enumerate(tally):
        print(type_string(i) + ":")
        for j, t in enumerate(type_tally):
            if t["count"] == 0:
                continue
            out = "{0} ({1}):    \t{2}dbm avg sig, \t{3} avg len"
            sig_sum = "{:10.4}".format(t["sig_sum"] / t["count"])
            len_sum = "{:10.4}".format(t["len_sum"] / t["count"])
            print(out.format(j, t["count"], sig_sum, len_sum))


def get_freq():
    """gets the frequency of the active channel in mhz"""
    FNULL = subprocess.DEVNULL
    iwcfg = subprocess.check_output('iwconfig', stderr=FNULL, close_fds=True)
    expr = args.iface + ".+Frequency:([\d.]+)\sGHz"
    ghz = re.search(re.compile(expr), iwcfg.decode("utf-8"))
    if ghz is None:
        return 2412
    return float(ghz.groups()[0]) * 1000


def start_channel_hopping(callback):
    """hops wifi channels in order, executing callback on them"""
    global args, chan, freq, stop_loop, MIN_CHANNEL_TIME, MAX_CHANNEL_TIME
    # channels = range(1, 14)   # what to use in a general case
    channels = [1, 2, 6]        # the most busy channels near our opening
    weights = [10, 1, 10]       # channel 2 is kinda dead
    pool = [chan for i, chan in enumerate(channels) for j in range(weights[i])]

    while not stop_loop:
        random.shuffle(pool)
        for channel in pool:
            try:
                subprocess.check_call(["iwconfig", args.iface, "channel",
                                       "%i" % channel])
                chan = channel
                freq = get_freq()
                timeout = random.uniform(MIN_CHANNEL_TIME, MAX_CHANNEL_TIME)
                sniff(store=0, prn=callback, iface=args.iface, timeout=timeout)
            except KeyboardInterrupt:
                stop_loop = True
                break


def get_dbm_antsignal_panda(notdecoded):
    return -(256-ord(notdecoded[-4:-3]))


def get_dbm_antsignal_tplink(notdecoded):
    return -(256-ord(notdecoded[-2:-1]))


"""
Now we begin the rest of the script
"""

args = None
stop_loop = False
chan = 1
freq = 2412
tally = []
transmitters = {}
do_tally = False
get_dbm_antsignal = get_dbm_antsignal_panda
try:
    args = get_args()
    osc_client = udp_client.SimpleUDPClient(args.osc_address, args.osc_port)
    do_tally = args.count > 0 & args.count < 100000
    cb = scapy_cb
    if args.tplink:
        get_dbm_antsignal = get_dbm_antsignal_tplink
    if do_tally:
        cb = scapy_cb_tally
        init_tally()
    freq = get_freq()

    if args.hop:
        start_channel_hopping(cb)
    else:
        sniff(store=0, prn=cb, count=args.count, iface=args.iface)
except KeyboardInterrupt:
    stop_loop = True
except Exception as e:
    stop_loop = True
    print(e)
finally:
    if do_tally:
        print_tally()
    quit()
