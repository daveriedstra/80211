#!/usr/bin/env bash

if [ -z "$1" ]; then
    echo "Usage: \nsudo ./start.sh [iface] [packet_count]"
    echo "if packet_count is empty, script will run indefinitely"
    exit 0
fi

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

DIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd $DIR
LOGS_DIR="logs"
JACKD_LOG="logs/jackd.log"
PD_LOG="logs/pd.log"
IFACE="$1"
PKT_COUNT=0
if [ ! -z "$2" ]; then
    PKT_COUNT=$2
fi

# ensure logs exist
if [ ! -d "$LOGS_DIR" ]; then
    mkdir $LOGS_DIR
fi

if [ ! -f "$JACKD_LOG" ]; then
    touch $JACKD_LOG
    chmod o+w $JACKD_LOG
    chmod g+w $JACKD_LOG
fi

if [ ! -f "$PD_LOG" ]; then
    touch $PD_LOG
    chmod o+w $PD_LOG
    chmod g+w $PD_LOG
fi


# set cleanup
cleanup() {
    echo "killing jackd and pd"
    killall puredata
    sleep 1
    killall jackd
    exit 0
}
trap 'cleanup' SIGINT


# performance hacks
mount -o remount,size=128M /dev/shm
for cpu in /sys/devices/system/cpu/cpu[0-9]*; do
	echo -n performance > $cpu/cpufreq/scaling_governor;
done


# set monitor mode
./setmode.sh $IFACE Monitor


#start jackd
export DISPLAY=:0
export `su pi -c "dbus-launch | grep ADDRESS"`
export `su pi -c "dbus-launch | grep PID"`

su $SUDO_USER -c "jackd -R -P70 -p16 -t2000 -v -dalsa -P -n 4 -p 2048 -r 32000 -s -S &> $JACKD_LOG &"

# give jackd some time to start
sleep 3

if pgrep "jackd" >/dev/null 2>&1 ; then
    echo "jackd started..."
else
    echo "Could not start jackd; check $JACKD_LOG for details"
    exit -1
fi


#start headless pd
su $SUDO_USER -c "puredata -nogui -rt -jack -nomidi -audioindev 0 -audiooutdev 1 -send 'dsp 1;' -open ./main.pd &> $PD_LOG &"

# give pd some time to start
sleep 3

if pgrep "puredata" >/dev/null 2>&1 ; then
    echo "pd started..."
else
    echo "Could not start puredata; check $PD_LOG for details"
    exit -1
fi


#start python script
./listen.py -i $IFACE -c $PKT_COUNT -b

# give pd some time to finish making sounds
sleep 3
echo "done listening" 
cleanup
